/* Copyright (C) 2005  Thomas Petazzoni

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
*/

#include <drivers/x86_videomem.h>
#include <drivers/kbd.h>
#include <drivers/tty.h>
#include <drivers/devices.h>

#include "console.h"

sos_ret_t
sos_console_subsystem_setup (void)
{
  sos_ret_t ret;
  struct tty_device *tty;

  ret = sos_screen_init();
  if (SOS_OK != ret)
    return ret;

  ret = tty_create (SOS_CHARDEV_CONSOLE_MINOR,
		    sos_screen_putchar, & tty);
  if (SOS_OK != ret)
    return ret;

  return sos_kbd_subsystem_setup (tty);
}
