/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_DEV_ZERO_H_
#define _SOS_DEV_ZERO_H_

/**
 * "Driver" to map /dev/zero in user space
 */


#include <sos/umem_vmm.h>


/**
 * Register the /dev/zero and /dev/null devices
 */
sos_ret_t sos_dev_zero_subsystem_setup(void);


/**
 * Map /dev/zero into user space
 * @note usage RESTRICTED to the kernel for start_init and the exec
 * syscall
 */
sos_ret_t sos_dev_zero_map(struct sos_umem_vmm_as * dest_as,
			   sos_uaddr_t *uaddr,
			   sos_size_t size,
			   sos_ui32_t access_rights,
			   sos_ui32_t flags);

#endif /* _SOS_EXEC_ELF32_H_ */
