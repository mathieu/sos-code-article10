/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_FS_VIRTFS_H_
#define _SOS_FS_VIRTFS_H_

#include <sos/errno.h>


/**
 * @file fs_virtfs.h
 *
 * Fake test file system. All the sos_fs_nodes and their contents are
 * stored in kernel memory. Thus, with "virtfs", the "disk" is
 * actually the kernel memory. In a sos_fs_node, the storage_location
 * field corresponds to the kernel address of the corresponding struct
 * sos_fs_node.
 */


/**
 * Solely register the "virtfs" FS type into the list of supported FS types
 */
sos_ret_t sos_fs_virtfs_subsystem_setup(void);

#endif
