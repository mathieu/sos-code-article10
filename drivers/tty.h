/* Copyright (C) 2005      David Decotigny, Thomas Petazzoni

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
*/
#ifndef _SOS_TTY_H_
#define _SOS_TTY_H_

#include <sos/types.h>

struct tty_device;

sos_ret_t tty_subsystem_setup (void);
sos_ret_t tty_subsystem_cleanup (void);

sos_ret_t tty_create (sos_ui32_t device_instance,
		      sos_ret_t (*write_func) (char c),
		      struct tty_device **tty_out);
sos_ret_t tty_remove (struct tty_device *tty);

/**
 * @note Function called without synchronization
 */
void tty_add_chars (struct tty_device *t, const char *s);

#endif

