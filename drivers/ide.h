/* Copyright (C) 2005  Thomas Petazzoni

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
*/
#ifndef _SOS_DRV_IDE_H_
#define _SOS_DRV_IDE_H_


/**
 * @file ide.h
 *
 * Basic PIO IDE implementation based on the ATA 3 & 4 standards
 * http://www.t13.org/
 */

sos_ret_t sos_ide_subsystem_setup (void);


#endif /* _SOS_DRV_IDE_H_ */
