/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_DEV_MEM_H_
#define _SOS_DEV_MEM_H_

/**
 * @file mem.h
 *
 * Drivers to map the kernel areas ("/dev/kmem") and the physical RAM
 * ("/dev/mem") into user space
 */

#include <sos/umem_vmm.h>


/**
 * Bind to the /dev/kmem and /dev/mem character devices
 */
sos_ret_t sos_dev_mem_chardev_setup(void);

#endif /* _SOS_EXEC_ELF32_H_ */
