/* Copyright (C) 2000  David Decotigny, Thomas Petazzoni, The KOS Team

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_SERIAL_H_
#define _SOS_SERIAL_H_

/**
 * @file serial.h
 *
 * 16550 Serial driver for SOS. This is a two-layers driver:
 *   - low-level layer: polling-style read/write functions
 *   - higher-level layer: interrupt-driver read function bound to a TTY device
 */

#include <sos/errno.h>
#include <sos/types.h>


/**
 * Create a new TTY device (minor SOS_CHARDEV_SERIAL_MINOR)
 * controlling the first serial line
 */
sos_ret_t sos_ttyS0_subsystem_setup (void);


#endif
