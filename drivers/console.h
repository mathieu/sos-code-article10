/* Copyright (C) 2005  Thomas Petazzoni

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
*/
#ifndef _SOS_DRV_CONSOLE_H_
#define _SOS_DRV_CONSOLE_H_

/**
 * @file console.h
 *
 * Simple tty device binding together the physical keyboard (@see
 * kbd.h) and the text-mode screen (@see x86_videomem.h)
 */

#include <sos/errno.h>


/**
 * Create a new TTY device (minor CHARDEV_CONSOLE_MINOR) that controls
 * the keyboard/screen
 */
sos_ret_t sos_console_subsystem_setup (void);

#endif /* _SOS_DRV_CONSOLE_H_ */
