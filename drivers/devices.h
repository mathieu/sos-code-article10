/* Copyright (C) 2005      David Decotigny, Thomas Petazzoni

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
*/
#ifndef _SOS_DEVICES_H_
#define _SOS_DEVICES_H_

/**
 *@file devices.h
 *
 * This file contains definitions concerning device drivers that must
 * be shared between userspace applications and the kernel. They are
 * mostly major numbers, IOCTL commands and parameters.
 *
 * This file must be built so that it can be safely included both from
 * user applications and the kernel.
 */

/* For /dev/zero & /dev/null devices */
#define SOS_CHARDEV_ZERO_MAJOR            1
#define SOS_CHARDEV_ZERO_MINOR            0
#define SOS_CHARDEV_NULL_MINOR            1

/* For memory char devices (/dev/mem and /dev/kmem) */
#define SOS_CHARDEV_MEM_MAJOR             2
#define SOS_CHARDEV_KMEM_MINOR            0 /* /dev/kmem */
#define SOS_CHARDEV_PHYSMEM_MINOR         1 /* /dev/mem */

/* TTY major and minor*/
#define SOS_CHARDEV_TTY_MAJOR             3
#define SOS_CHARDEV_CONSOLE_MINOR         0
#define SOS_CHARDEV_SERIAL_MINOR          1

/* TTY IOCTL commands */
#define SOS_IOCTL_TTY_SETPARAM            1
#define SOS_IOCTL_TTY_RESETPARAM          2

/* TTY IOCTL command parameters for SOS_IOCTL_TTY_SETPARAM and
   SOS_IOCTL_TTY_RESETPARAM */
#define SOS_IOCTLPARAM_TTY_CANON          1
#define SOS_IOCTLPARAM_TTY_ECHO           2

/** IDE major */
#define SOS_BLOCKDEV_IDE_MAJOR            1

/** Generic hardrive IOCTL: flush caches to disk */
#define SOS_IOCTL_BLOCKDEV_SYNC           1


#endif /* _SOS_DEVICE_H_ */
