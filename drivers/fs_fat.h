/* Copyright (C) 2007 Anthoine Bourgeois

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_FS_FAT_H_
#define _SOS_FS_FAT_H_

#include <sos/errno.h>


/**
 * @file fs_fat.h
 *
 * Basic implementation of the FAT16/32 file system for SOS.
 */


/**
 * Solely register the "FAT" FS type into the list of supported FS types
 */
sos_ret_t sos_fs_fat_subsystem_setup(void);

#endif
