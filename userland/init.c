/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/

#include <libc.h>
#include <debug.h>
#include <drivers/devices.h>
#include "fstest_utils.h"

/**
 * @file init.c
 * Test fork and exec()
 */

int main(void)
{
  bochs_printf("init: Welcome in userland !\n");


  /* Creating initial device nodes */
  TEST_EXPECT_CONDITION(mkdir("/dev", S_IRWXALL), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/zero", S_IRUSR | S_IWUSR,
			      S_IFCHR, SOS_CHARDEV_ZERO_MAJOR, SOS_CHARDEV_ZERO_MINOR), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/null", S_IRUSR | S_IWUSR,
			      S_IFCHR, SOS_CHARDEV_ZERO_MAJOR, SOS_CHARDEV_NULL_MINOR), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/kmem", S_IRUSR | S_IWUSR,
			      S_IFCHR, SOS_CHARDEV_MEM_MAJOR, SOS_CHARDEV_KMEM_MINOR), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/mem", S_IRUSR | S_IWUSR,
			      S_IFCHR, SOS_CHARDEV_MEM_MAJOR, SOS_CHARDEV_PHYSMEM_MINOR), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/tty", S_IRUSR | S_IWUSR,
			      S_IFCHR, SOS_CHARDEV_TTY_MAJOR, SOS_CHARDEV_CONSOLE_MINOR), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/ttyS", S_IRUSR | S_IWUSR,
			      S_IFCHR, SOS_CHARDEV_TTY_MAJOR, SOS_CHARDEV_SERIAL_MINOR), RETVAL == 0);

  TEST_EXPECT_CONDITION(mknod("/dev/hda", S_IRUSR | S_IWUSR,
			      S_IFBLK, SOS_BLOCKDEV_IDE_MAJOR, 0), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/hda1", S_IRUSR | S_IWUSR,
			      S_IFBLK, SOS_BLOCKDEV_IDE_MAJOR, 1), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/hda2", S_IRUSR | S_IWUSR,
			      S_IFBLK, SOS_BLOCKDEV_IDE_MAJOR, 2), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/hda3", S_IRUSR | S_IWUSR,
			      S_IFBLK, SOS_BLOCKDEV_IDE_MAJOR, 3), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/hda4", S_IRUSR | S_IWUSR,
			      S_IFBLK, SOS_BLOCKDEV_IDE_MAJOR, 4), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/hda5", S_IRUSR | S_IWUSR,
			      S_IFBLK, SOS_BLOCKDEV_IDE_MAJOR, 5), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/hda6", S_IRUSR | S_IWUSR,
			      S_IFBLK, SOS_BLOCKDEV_IDE_MAJOR, 6), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/hda7", S_IRUSR | S_IWUSR,
			      S_IFBLK, SOS_BLOCKDEV_IDE_MAJOR, 7), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/hdb", S_IRUSR | S_IWUSR,
			      S_IFBLK, SOS_BLOCKDEV_IDE_MAJOR, 16), RETVAL == 0);
  TEST_EXPECT_CONDITION(mknod("/dev/hdb1", S_IRUSR | S_IWUSR,
			      S_IFBLK, SOS_BLOCKDEV_IDE_MAJOR, 17), RETVAL == 0);

  ls("/", 1, 1);

  /* Set up the shell on the console */
  TEST_EXPECT_CONDITION(open("/dev/tty", O_RDWR), RETVAL == 0);
  TEST_EXPECT_CONDITION(open("/dev/tty", O_RDWR), RETVAL == 1);
  TEST_EXPECT_CONDITION(open("/dev/tty", O_RDWR), RETVAL == 2);

  if (fork() == 0)
    exec ("shell");

  close (2);
  close (1);
  close (0);

  /* Set up the shell on the serial port */
  TEST_EXPECT_CONDITION(open("/dev/ttyS", O_RDWR), RETVAL == 0);
  TEST_EXPECT_CONDITION(open("/dev/ttyS", O_RDWR), RETVAL == 1);
  TEST_EXPECT_CONDITION(open("/dev/ttyS", O_RDWR), RETVAL == 2);

  if (fork() == 0)
    exec ("shell");

  close (2);
  close (1);
  close (0);



  bochs_printf("init: The end\n");
  return 0;
}
