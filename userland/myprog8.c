/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/

#include <libc.h>
#include <stdarg.h>
#include <debug.h>


/**
 * @file myprog8.c
 * Make sure that shared mappings (here: anonymous shared mapping) are
 * really shared between father and child
 */

int main(void)
{
  int i;
  char *zoup;
  int fd;

  fd = open("/dev/zero", O_RDWR);
  zoup = mmap((void*)4096, 8*1024*1024,
	      PROT_READ | PROT_WRITE,
	      MAP_SHARED,
	      fd, 0x123456789012345ULL);
  close(fd);

  bochs_printf("mapped @%x\n", (unsigned)zoup);

  switch (fork())
    {
    case 0:
      bochs_printf("CHILD 1: %d %d\n", zoup[42], zoup[43]);
      for (i = 0 ; i < 1000000 ; i++)
	zoup[43] = i / 19;
      bochs_printf("CHILD 2: %d %d\n", zoup[42], zoup[43]);
      for (i = 0 ; i < 1000000 ; i++)
	zoup[43] = i / 19;
      bochs_printf("CHILD 3: %d %d\n", zoup[42], zoup[43]);
      break;

    default:
      bochs_printf("FATHER 1: %d %d\n", zoup[42], zoup[43]);
      for (i = 0 ; i < 1000000 ; i++)
	zoup[42] = i / 17;
      bochs_printf("FATHER 2: %d %d\n", zoup[42], zoup[43]);
      break;
    }

  return 0;
}
