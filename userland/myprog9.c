/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/

#include <crt.h>
#include <libc.h>
#include <stdarg.h>
#include <debug.h>

/**
 * @file myprog9.c
 *
 * mmap/munmap tests
 *
 * We use the temporary syscall 4004 to dump the list of VRs in the
 * thread's address space
 */

int main(void)
{
  char *zoup;
  int fd;

  fd = open("/dev/zero", O_RDWR);
  zoup = mmap((void*)4096, 8*1024*1024,
	      PROT_READ | PROT_WRITE,
	      MAP_SHARED,
	      fd, 34);
  close(fd);

  bochs_printf("mapped @%x\n", (unsigned)zoup);

  /* Do a fork() here to complicate things */
  fork();
  fork();
  
  _sos_syscall1(4004, (unsigned)"Initial");
  munmap(zoup, 10*4096);
  zoup += 10*4096;
  _sos_syscall1(4004, (unsigned)"After munmap Low");

  munmap(zoup-4096, 2*4096);
  zoup += 4096;
  _sos_syscall1(4004, (unsigned)"After munmap Before Low");

  munmap(zoup-4096, 4096);
  _sos_syscall1(4004, (unsigned)"After munmap Before Low (bis)");

  munmap(zoup + 1024*1024, 4096);
  _sos_syscall1(4004, (unsigned)"After munmap middle");

  munmap(zoup + 1024*1024, 4096);
  _sos_syscall1(4004, (unsigned)"After munmap middle (bis)");

  munmap(zoup + 8*1024*1024 - 11*4096 - 4096, 4096);
  _sos_syscall1(4004, (unsigned)"After munmap High");

  munmap(zoup + 8*1024*1024 - 11*4096 - 2*4096, 3*4096);
  _sos_syscall1(4004, (unsigned)"After munmap Past High");

  munmap((void*)0x40000000, 0x10000000);
  _sos_syscall1(4004, (unsigned)"After munmap Over everything");

  munmap((void*)0x40000000, 0x10000000);
  _sos_syscall1(4004, (unsigned)"After munmap Over everything (bis)");

  return 0;
}
