/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/

#include <debug.h>

/**
 * @file myprog5.c
 * Test inifite loop (preemptive scheduling of threads in user mode)
 */

int main(void)
{
  int retval;

  bochs_printf("Prog5: Hello world 1 !\n");
  for (;;)
    ;
  bochs_printf("Prog5: Hello world 2 !\n");

  return retval;
}
