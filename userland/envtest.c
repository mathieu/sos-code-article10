#include <libc.h>

int main(int argc, char * argv[], char *envp[])
{
  int i;
  char **e;

  printf("Nargs: %d\n", argc);
  for (i = 0 ; i < argc ; i ++)
    printf("  arg[%d] = |%s|\n", i, argv[i]);

  for (e = envp ; e && *e ; e ++)
    printf("  ENV |%s|\n", *e);

  return 0;
}
