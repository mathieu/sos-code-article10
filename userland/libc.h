/* Copyright (C) 2005 David Decotigny
   Copyright (C) 2003 Thomas Petazzoni

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_USER_LIBC_H_
#define _SOS_USER_LIBC_H_

#include <types.h>

/**
 * @file libc.h
 *
 * The basic user C library for user programs
 */

/**
 * The most important function of a C program ! ;)
 */
void exit (int status);


/**
 * Function to duplicate the current running process
 */
pid_t fork(void);


/**
 * Retrieves the current process's PID
 */
pid_t getpid(void);


/**
 * Function to re-initialize the address space of the current process
 * with that of the program 'progname'
 */
int execve(const  char  *filename,
	   char  *const argv [],
	   char *const envp[]);
int execv(const char  * filename,
	  char  * const argv []);
int execl(const  char  *path, ...);
int execle(const  char  *path, ...);


/**
 * Equivalent to execl(filename, filename);
 */
int exec(char * const filename);


int nanosleep(unsigned long int sec,
	      unsigned long int nanosec);


int sleep(unsigned int seconds);


/**
 * Environment variables management API
 */
extern char **environ; /** Needed by execl */

int putenv(char * string);
int setenv(const char *name, const char *value, int overwrite);
char *getenv(const char *name);
void unsetenv(const char *name);
int clearenv(void);


/**
 * These flags (for mmap API) MUST be consistent with those defined in
 * paging.h and umem_vmm.h
 */
#define PROT_NONE  0
#define PROT_READ  (1<<0)
#define PROT_WRITE (1<<1)
#define PROT_EXEC  (1<<2) /* Not supported on IA32 */

#define MAP_PRIVATE (0 << 0)
#define MAP_SHARED  (1 << 0)
#define MAP_FIXED   (1 << 31)


/**
 * Unmap the given user address interval
 */
int munmap(void * start, size_t length);


/**
 * Change the access permissions of the given user address interval
 */
int mprotect(const void *addr, size_t len, int prot);


/**
 * This flag tells mremap that the underlying VR may be moved, when necessary
 */
#define MREMAP_MAYMOVE (1 << 30)

/**
 * Grow/shrink the end of the given mapping
 */
void * mremap(void * old_addr, size_t old_len,
	      size_t new_len,
	      unsigned long flags);


/** Flags for msync */
#define MS_SYNC  (1 << 0)
#define MS_ASYNC (0 << 0)

/** Synchronize cache contents with mapped memory */
int msync(void *start, size_t length, int flags);


/**
 * Standard heap management API
 */

/** @note MT UNSAFE */
int brk(void *end_data_seg);

/** @note MT UNSAFE */
void *sbrk(ptrdiff_t increment);

/** @note MT UNSAFE */
void * calloc (size_t nmemb, size_t size);

/** @note MT UNSAFE */
void * malloc (size_t size);

/** @note Does nothing (not implemented yet) */
void free(void *ptr);


/*
 * Filesystem subsystem
 */

int mount(const char *source, const char *target,
	  const char *filesystemtype, unsigned long mountflags,
	  const char *data);


int umount(const char *target);


void sync(void);


struct statvfs
{
  unsigned int  f_rdev_major;
  unsigned int  f_rdev_minor;
  size_t        f_sz_total;  /**< Total size */
  size_t        f_sz_free;   /**< Size left on device */
  unsigned int  f_node_total;/**< Total allocatable FS nodes */
  unsigned int  f_node_avail;/**< Number of available free FS nodes */
  unsigned int  f_flags;
};

int statvfs(const char *path, struct statvfs *buf);


#define O_EXCL      (1 << 0)
#define O_CREAT     (1 << 1)
#define O_TRUNC     (1 << 2)
#define O_NOFOLLOW  (1 << 3)
#define O_DIRECTORY (1 << 4) /* Incompatible with CREAT/TRUNC */
#define O_SYNC      (1 << 5)

#define O_RDONLY    (1 << 16)
#define O_WRONLY    (1 << 17)
#define O_RDWR      (O_RDONLY | O_WRONLY)

/**
 * FS access rights. Same as in kernel fs.h
 */
#define S_IRUSR     00400
#define S_IWUSR     00200
#define S_IXUSR     00100
#define S_IRWXALL   07777 /* For symlinks */

int open(const char *pathname, int flags, /* mode_t mode */...);

int close(int fd);


int read(int fd, char * buf, size_t len);
int write(int fd, const char * buf, size_t len);

/* Same as sos_seek_whence_t in kernel fs.h */
#define SEEK_SET 42
#define SEEK_CUR 24
#define SEEK_END 84
off_t lseek(int fd, off_t offset, int whence);
loff_t lseek64(int fd, loff_t offset, int whence);
void * mmap(void *start, size_t length, int prot , int flags,
	    int fd, loff_t offset);
int ftruncate(int fd, off_t length);
int ftruncate64(int fd, loff_t length);
int fcntl(int fd, int cmd, int arg);
int ioctl(int fd, int cmd, int arg);

int creat(const char *pathname, mode_t mode);
int link (const char *oldpath, const char *newpath);
int unlink(const char *pathname);
int rename(const char *oldpath, const char *newpath);
int symlink(const char *target, const char *path);

/* Same as sos_fs_node_type_t in kernel fs.h */
#define S_IFREG 0x42
#define S_IFDIR 0x24
#define S_IFLNK 0x84
#define S_IFCHR 0x48
#define S_IFBLK 0x12
int mknod(const char *pathname, mode_t mode,
	  int type,
	  unsigned int major, unsigned minor);


int mkdir(const char *pathname, mode_t mode);


int rmdir(const char *pathname);


int chmod(const char *path, mode_t mode);


struct dirent
{
  unsigned long long int storage_location;
  unsigned long long int offset_in_dirfile;
  unsigned int type;
  unsigned short namelen;

#define SOS_FS_DIRENT_NAME_MAXLEN 128
  char       name[SOS_FS_DIRENT_NAME_MAXLEN];
};

/* Forward declaration (defined in libc.c) */
struct sos_DIR_struct;
#define DIR struct sos_DIR_struct

DIR *opendir(const char *name);
int dirfd(const DIR * dir);
struct dirent *readdir(DIR *dir);
int closedir(DIR *dir);

struct stat
{
  unsigned int           st_rdev_major;
  unsigned int           st_rdev_minor;
  int                    st_type;
  unsigned long long int st_storage_location;
  int                    st_access_rights;
  unsigned int           st_nlink;
  signed long long int   st_size;
};

int stat(const char *file_name, struct stat *buf);
int lstat(const char *file_name, struct stat *buf);
int chroot(const char *path);
int chdir(const char *path);
int fchdir(int fd);

int printf(const char *, ...);

#endif /* _SOS_USER_LIBC_H_ */
