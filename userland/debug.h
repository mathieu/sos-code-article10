/* Copyright (C) 2005 David Decotigny
   Copyright (C) 2003 Thomas Petazzoni

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_LIBC_DEBUG_H_
#define _SOS_LIBC_DEBUG_H_

/**
 * @file debug.h
 *
 * Non standard debugging features
 */

/**
 * Non standard function to print something on bochs
 */
int bochs_printf(const char *format, /* args */...)
     __attribute__ ((format (printf, 1, 2)));

#endif /* _SOS_USER_LIBC_H_ */
