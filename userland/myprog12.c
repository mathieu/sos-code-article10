/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/

#include <crt.h>
#include <libc.h>
#include <string.h>
#include <stdarg.h>
#include <debug.h>


/**
 * @file myprog12.c
 *
 * /dev/mem & /dev/kmem tests
 */


int main(void)
{
  char *zoup;

  /* Test /dev/mem */

  /* Map the x86 text framebuffer into this process address space in
     shared mode */
  int fd;

  fd = open("/dev/mem", O_RDWR);
  zoup = mmap(0, 4096, PROT_READ | PROT_WRITE,
	      MAP_SHARED,
	      fd, 0xb8000);
  close(fd);

  bochs_printf("mapped mem @%x\n", (unsigned)zoup);  
  _sos_syscall1(4004, (unsigned)"Apres mmap video");

  /* Do some forks to complicate things */
  fork();
  fork();

  /* Write a string into it: it should be printed on screen */
  strzcpy(zoup + 80*5*2 + 10*2,
	  "H e l l o   W o r l d   f r o m   S O S   i n   U s e r   M o d e ",
	  250);

  /* Map the x86 text framebuffer into this process address space in
     PRIVATE mode */
  fd = open("/dev/mem", O_RDWR);
  zoup = mmap(0, 4096, PROT_READ | PROT_WRITE,
	      0 /* Private */,
	      fd, 0xb8000);
  close(fd);
  bochs_printf("mapped mem @%x\n", (unsigned)zoup);  
  _sos_syscall1(4004, (unsigned)"Apres mmap video");

  /* Do some forks to complicate things */
  fork();
  fork();

  /* Write a string into it: it should NOT be printed on screen since
     the mapping is private */
  strzcpy(zoup + 80*5*2 + 10*2,
	  "Y o u   c a n n o t   S e e   T h i s ! ",
	  250);

  /* Test /dev/kmem */

  /* remap some kernel pages in user space. We'd better map this in
     "private" mode here, this way we can do whatever we like in this
     area. If we'd mapped it read/write, this would overwrite kernel
     data/code, causing a crash sooner or later. */
  fd = open("/dev/kmem", O_RDWR);
  zoup = mmap(0, 100*4096, PROT_READ | PROT_WRITE,
	      0 /* private */,
	      fd, 0x00201000);
  close(fd);
  bochs_printf("mapped kmem @%x\n", (unsigned)zoup);  
  _sos_syscall1(4004, (unsigned)"Apres mmap kernel");

  /* Do some forks to complicate things */
  fork();
  fork();

  /* Overwriting our private "copy" of kernel */
  memset(zoup, 0x0, 10*4096);
  _sos_syscall1(4004, (unsigned)"Apres memset kernel");

  return 0;
}
