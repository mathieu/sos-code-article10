/* Copyright (C) 2003  The KOS Team
   Copyright (C) 1999  Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_LIBC_TYPES_H_
#define _SOS_LIBC_TYPES_H_

/**
 * @file types.h
 */

typedef unsigned int size_t;

typedef unsigned int addr_t;
typedef int ptrdiff_t;
typedef int pid_t;
typedef signed long int off_t;
typedef signed long long int loff_t;

typedef unsigned int mode_t;
typedef unsigned long long int dev_t;

#ifndef NULL
#  define NULL ((void*)0)
#endif

#ifndef TRUE
#  define TRUE (!0)
#endif

#ifndef FALSE
#  define FALSE (!TRUE)
#endif

#endif /* _SOS_LIBC_TYPES_H_ */
