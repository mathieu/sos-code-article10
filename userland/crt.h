/* Copyright (C) 2005 David Decotigny
   Copyright (C) 2003 Thomas Petazzoni

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_USER_CRT_H_
#define _SOS_USER_CRT_H_


/**
 * @file crt.h
 *
 * C runtime environment for the user side of the kernel/user
 * interface
 */

#include <types.h>

/**
 * We include the syscall.h file from the kernel to retrieve the list
 * of available syscall ID
 */
#include <sos/syscall.h>


/**
 * The syscall wrappers hiding the details of the implementation
 */
int _sos_syscall0(int id);

int _sos_syscall1(int id,
		  unsigned int arg1);

int _sos_syscall2(int id,
		  unsigned int arg1,
		  unsigned int arg2);

int _sos_syscall3(int id,
		  unsigned int arg1,
		  unsigned int arg2,
		  unsigned int arg3);

int _sos_syscall4(int id,
		  unsigned int arg1,
		  unsigned int arg2,
		  unsigned int arg3,
		  unsigned int arg4);

int _sos_syscall5(int id,
		  unsigned int arg1,
		  unsigned int arg2,
		  unsigned int arg3,
		  unsigned int arg4,
		  unsigned int arg5);

int _sos_syscall6(int id,
		  unsigned int arg1,
		  unsigned int arg2,
		  unsigned int arg3,
		  unsigned int arg4,
		  unsigned int arg5,
		  unsigned int arg6);

int _sos_syscall7(int id,
		  unsigned int arg1,
		  unsigned int arg2,
		  unsigned int arg3,
		  unsigned int arg4,
		  unsigned int arg5,
		  unsigned int arg6,
		  unsigned int arg7);

int _sos_syscall8(int id,
		  unsigned int arg1,
		  unsigned int arg2,
		  unsigned int arg3,
		  unsigned int arg4,
		  unsigned int arg5,
		  unsigned int arg6,
		  unsigned int arg7,
		  unsigned int arg8);


/**
 * The most important function of a C program ! ;)
 */
void _sos_exit (int status) __attribute__((noreturn));


/**
 * Non standard function to print something on bochs
 */
int _sos_bochs_write(const char *str, unsigned length);


/**
 * Syscall to duplicate the current running process
 */
int _sos_fork(void);


/**
 * Syscall to retrieved the current process's PID
 */
int _sos_getpid(void);


/**
 * Syscall to re-initialize the address space of the current process
 * with that of the program 'progname'
 *
 * The args argument is the address of an array that contains the
 * arguments and environnement variables for the new process.
 */
int _sos_exec(const char * prog,
	      void const* args,
	      size_t arglen);


/**
 * Syscall to unmap the given user address interval
 */
int _sos_munmap(void * start, size_t length);


/**
 * Syscall to change the access permissions of the given user address
 * interval
 */
int _sos_mprotect(const void *addr, size_t len, int prot);


/**
 * Syscall to resize the given user mapped resource at the given
 * old_addr address
 */
int _sos_mresize(void * old_addr, size_t old_len,
		 void * *new_addr, size_t new_len,
		 unsigned long flags);


/**
 * Syscall to synchronize caches with the contents of the mapped memory
 */
int _sos_msync(void *start, size_t length, int flags);


/**
 * Definition of a thread routine
 */
typedef void (sos_thread_func_t(unsigned long int arg));

/**
 * Syscall to create a new thread inside the current process
 */
int _sos_new_thread(sos_thread_func_t *func,
		    void * arg,
		    size_t stack_size);


/**
 * Syscall to put the calling thread asleep
 */
int _sos_nanosleep(unsigned long int sec,
		   unsigned long int nanosec);


/**
 * Syscall to get/set heap top address
 */
void * _sos_brk(void * new_top_address);


int _sos_mount(const char *source, const char *target,
	       const char *filesystemtype, unsigned long mountflags,
	       const char *data);
int _sos_umount(const char *target);

void _sos_sync(void);

struct statvfs; /**< Forward declaration */
int _sos_statvfs(const char *path, struct statvfs *buf);

int _sos_open(const char * pathname, int flags, int mode);
int _sos_close(int fd);

int _sos_read(int fd, char * buf, size_t * len);
int _sos_write(int fd, const char * buf, size_t * len);
int _sos_seek64(int fd, loff_t * offset, int whence);
int _sos_ftruncate64(int fd, loff_t length);
int _sos_fmmap(void ** ptr_hint_addr, size_t len, int prot, int flags,
	       int fd, loff_t offset);
int _sos_fcntl(int fd, int cmd, int arg);
int _sos_ioctl(int fd, int cmd, int arg);

struct dirent; /* Forward declaration */
int _sos_readdir(int fd, struct dirent * dirent);

int _sos_creat(const char *pathname, int mode);
int _sos_link (const char *oldpath, const char *newpath);
int _sos_unlink(const char *pathname);
int _sos_rename (const char *oldpath, const char *newpath);
int _sos_symlink(const char *target, const char *path);
int _sos_mknod(const char *pathname, mode_t mode,
	       int type,
	       unsigned int major, unsigned minor);

int _sos_mkdir(const char *pathname, mode_t mode);
int _sos_rmdir(const char *pathname);

int _sos_chmod(const char *pathname, mode_t mode);

struct stat; /**< forward declaration */
int _sos_stat(const char *pathname, int nofollow, struct stat * st);

int _sos_chroot(const char *dirname);
int _sos_chdir(const char *dirname);
int _sos_fchdir(int fd);

#endif /* _SOS_USER_CRT_H_ */
