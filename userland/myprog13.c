/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/

#include <crt.h>
#include <libc.h>
#include <stdarg.h>
#include <debug.h>


/**
 * @file myprog13.c
 * Test brk (and, hence: mremap)
 */

int main(void)
{
  char * zoup;

  bochs_printf("myprog13: Hello world !\n");

  /* Do some forks to complicate things */
  fork();
  fork();

  zoup = _sos_brk(0);
  bochs_printf("_sos_brk(0)=%x\n", (unsigned)_sos_brk(0));
  bochs_printf("_sos_brk(+0)=%x\n", (unsigned)_sos_brk(zoup));
  bochs_printf("_sos_brk(+1)=%x\n", (unsigned)_sos_brk(zoup + 1));
  zoup[3] = 42;
  bochs_printf("z[] = %d\n", zoup[3]);
  bochs_printf("_sos_brk(0)=%x\n", (unsigned)_sos_brk(0));
  bochs_printf("_sos_brk(orig)=%x\n", (unsigned)_sos_brk(zoup));
  bochs_printf("_sos_brk(0)=%x\n", (unsigned)_sos_brk(0));
  //  zoup[3] = 42;
  
  bochs_printf("brk(0)=%x\n", (unsigned)brk(0));
  bochs_printf("sbrk(0)=%x\n", (unsigned)sbrk(0));
  bochs_printf("brk(0)=%x\n", (unsigned)brk(0));
  bochs_printf("sbrk(0)=%x\n", (unsigned)sbrk(0));
  bochs_printf("sbrk(1)=%x\n", (unsigned)sbrk(1));
  bochs_printf("brk(0)=%x\n", (unsigned)brk(0));
  bochs_printf("sbrk(0)=%x\n", (unsigned)sbrk(0));
  zoup = sbrk(0);
  bochs_printf("brk(+1)=%x\n", (unsigned)brk(zoup + 1));
  bochs_printf("sbrk(0)=%x\n", (unsigned)sbrk(0));
  bochs_printf("brk(0)=%x\n", (unsigned)brk(0));
  bochs_printf("sbrk(0)=%x\n", (unsigned)sbrk(0));

  bochs_printf("sbrk(-4k)=%x\n", (unsigned)sbrk(-4096));
  bochs_printf("brk(0)=%x\n", (unsigned)brk(0));
  bochs_printf("sbrk(0)=%x\n", (unsigned)sbrk(0));
  zoup = sbrk(0);
  bochs_printf("brk(-4k)=%x\n", (unsigned)brk(zoup - 4096));
  bochs_printf("brk(0)=%x\n", (unsigned)brk(0));
  bochs_printf("sbrk(0)=%x\n", (unsigned)sbrk(0));

  bochs_printf("malloc(0)=%x\n", (unsigned)malloc(0));
  bochs_printf("malloc(1)=%x\n", (unsigned)malloc(1));
  bochs_printf("malloc(2)=%x\n", (unsigned)malloc(2));
  bochs_printf("malloc(3)=%x\n", (unsigned)malloc(3));
  bochs_printf("malloc(4)=%x\n", (unsigned)malloc(4));
  bochs_printf("malloc(5)=%x\n", (unsigned)malloc(5));
  bochs_printf("malloc(6)=%x\n", (unsigned)malloc(6));
  bochs_printf("malloc(7)=%x\n", (unsigned)malloc(7));
  bochs_printf("malloc(8)=%x\n", (unsigned)malloc(8));
  bochs_printf("malloc(9)=%x\n", (unsigned)malloc(9));
  bochs_printf("malloc(0x30000)=%x\n", (unsigned)malloc(0x30000));
  bochs_printf("malloc(1)=%x\n", (unsigned)malloc(1));

  bochs_printf("myprog13: The end\n");
  return 0;
}
