/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/

#include <libc.h>
#include <stdarg.h>
#include <debug.h>

static unsigned rien[16384];


/**
 * @file myprog7.c
 * Tests forks and make sure that private mappings (eg the '.data'
 * section of the program object file) are really private
 */


static void assign(unsigned * addr, const unsigned val)
{
  bochs_printf("[0x%x]=0x%x <- 0x%x\n", (unsigned)addr, *addr, val);
  *addr = val;
  bochs_printf(" ===> 0x%x\n", *addr);
}


int main(void)
{
  int i, level;
  
  bochs_printf("myprog7: Hello\n");

  assign(& rien[0], 0x42);
  assign(& rien[1000], 0x42);

  level = 0;
  for (i = 0 ; i < 10 ; i++)
    {
      pid_t fork_status;

      fork_status = fork();
      if (fork_status == 0)
	{
	  level ++;
	  bochs_printf("myprog7: I am the child %d %d\n", level, i);

	  assign(& rien[0], 0x0f<<24 | (level << 8) | i);
	  assign(& rien[1000], 0x0a<<24 | (level << 8) | i);

	  if ((i % 2) == 0)
	    {
	      char strprog[20];
	      snprintf(strprog, sizeof(strprog), "myprog%d", i/2 + 1);
	      exec(strprog);
	    } 
	}
      else if (fork_status > 0)
	{
	  bochs_printf("myprog7: I am the father %d %d\n", level, i);

	  assign(& rien[0], 0x8f<<24 | (level << 8) | i);
	  assign(& rien[1000], 0x8a<<24 | (level << 8) | i);
	}
      else
	bochs_printf("myprog7: fork error %d !\n", fork_status);
    }

  bochs_printf("myprog7: The end %d %d\n", level, i);
  assign(& rien[0], 0xff<<24 | (level << 8) | i);
  assign(& rien[1000], 0xfa<<24 | (level << 8) | i);

  return 0;
}
