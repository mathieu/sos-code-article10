/* Copyright (C) 2005  David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_FSTEST_UTILS_H_
#define _SOS_FSTEST_UTILS_H_

/**
 * @file fstest.h
 *
 * Macro and support functions for the fstest programs
 */

#define TEST_EXPECT_CONDITION(code,condition) \
  ({ static char * _str_ok = "[32mPASSED[m"; \
     static char * _str_failed = "[31;1mFAILED[m"; \
     int RETVAL = (int)(code); \
     int _verdict = (int)(condition); \
     bochs_printf("L%d [34m%s[m: %s %s (retval=%p aka %d)\n", \
       __LINE__, #code, #condition, \
       (_verdict)?_str_ok:_str_failed, (void*)RETVAL, RETVAL); \
     _verdict; })


/** Dump the list of files to the bochs debugging output */
void ls(const char * path, int detailed, int recursive);

#endif /* _SOS_FSTEST_UTILS_H_ */
