/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/

#include <crt.h>
#include <libc.h>
#include <stdarg.h>
#include <debug.h>

/**
 * @file myprog10.c
 *
 * mprotect tests
 *
 * We use the temporary syscall 4004 to dump the list of VRs in the
 * thread's address space
 */

int main(void)
{
  char * zoup;
  int fd;

  fd = open("/dev/zero", O_RDWR);
  zoup = mmap((void*)4096, 8*1024*1024,
	      PROT_READ | PROT_WRITE,
	      MAP_SHARED,
	      fd, 34);
  close(fd);

  bochs_printf("mapped @%x\n", (unsigned)zoup);

  /* Do some forks to complicate things */
  fork();
  fork();

  _sos_syscall1(4004, (unsigned)"Initial");
  mprotect(zoup, 10*4096, PROT_READ);
  zoup += 10*4096;
  _sos_syscall1(4004, (unsigned)"After mprotect Low");

  mprotect(zoup-4096, 2*4096, PROT_READ);
  zoup += 4096;
  _sos_syscall1(4004, (unsigned)"After mprotect Before Low");

  mprotect(zoup-4096, 4096, PROT_READ);
  _sos_syscall1(4004, (unsigned)"After mprotect Before low (bis)");

  mprotect(zoup + 1024*1024, 4096, PROT_READ);
  _sos_syscall1(4004, (unsigned)"After mprotect Middle");

  mprotect(zoup + 1024*1024, 4096, PROT_READ);
  _sos_syscall1(4004, (unsigned)"After mprotect Middle (bis)");

  mprotect(zoup + 8*1024*1024 - 11*4096 - 4096, 4096, PROT_READ);
  _sos_syscall1(4004, (unsigned)"After mprotect High");

  mprotect(zoup + 8*1024*1024 - 11*4096 - 2*4096, 3*4096, PROT_READ);
  _sos_syscall1(4004, (unsigned)"After mprotect Past High");

  mprotect((void*)0x40000000, 0x10000000, PROT_READ);
  _sos_syscall1(4004, (unsigned)"After mprotect Complete VR");

  mprotect((void*)0x40000000, 0x10000000, PROT_READ);
  _sos_syscall1(4004, (unsigned)"After mprotect Complete VR (bis)");

  return 0;
}
