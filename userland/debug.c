/* Copyright (C) 2004  David Decotigny (with INSA Rennes for vsnprintf)
   Copyright (C) 2003  The KOS Team
   Copyright (C) 1999  Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#include <crt.h>
#include <stdarg.h>

#include "debug.h"

int bochs_printf(const char *format, /* args */...)
{
  char buff[4096];
  int len;
  va_list ap;
  
  va_start(ap, format);
  len = vsnprintf(buff, sizeof(buff), format, ap);
  va_end(ap);
  
  if (len < 0)
    return len;

  return _sos_bochs_write(buff, len);
}
