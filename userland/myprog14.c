/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/

#include <crt.h>
#include <libc.h>
#include <stdarg.h>
#include <debug.h>

/**
 * @file myprog14.c
 *
 * uaccess tests. Demonstrate that a page fault in kernel during
 * userspace access (with uaccess.c functions) is NEVER (or at least
 * should not be) fatal:
 *  - licit page faults are resolved as usual (COW, page_in, ...)
 *  - illicit page faults are caught and the uaccess functions tolerate
 *    and report it
 *
 * We use the temporary syscall 4012 (hexdump) to force the kernel to
 * call user_memcpy and do some page faults.
 */

int main(void)
{
  char * zoup;
  int fd;

  fd = open("/dev/zero", O_RDWR);
  zoup = mmap(0, 8192,
	      PROT_READ | PROT_WRITE,
	      MAP_SHARED,
	      fd, 34);
  close(fd);

  bochs_printf("mapped @%x\n", (unsigned)zoup);

  /* Do some forks to complicate things */
  fork();
  fork();

  /* Force the first page of the mapping to be allocated */
  *zoup = 'a';

  _sos_syscall2(4012, (unsigned)zoup, 16384);
  /*
   * all 3 cases are handled here:
   *  - [zoup, zoup + 4kB[: no page fault at all (page already mapped)
   *  - [zoup+4kB, zoup + 8kB[: page fault => /dev/zero page_in() called
   *  - [zoup+8kB, ...[: page fault => illegal user address =>
   *    user_memcpy reports that only 8kB out of the 16kB could be
   *    hexdumped
   */

  return 0;
}
