/* Copyright (C) 2004 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_SCHED_H_
#define _SOS_SCHED_H_


/**
 * @file sched.h
 *
 * A basic scheduler inspired from the O(1) Linux scheduler. Supports
 * 2 classes of thread priorities:
 *  - so-called 'real-time' threads scheduled according to a simple
 *    traditional static priority real-time scheduler. "Real-time" round
 *    robin scheduling is not supported.
 * - "fair" time-sharing scheduling for non real-time threads. "fair"
 *    because no starvation among the non real-time threads is
 *    possible. Contrary to the original O(1) Linux scheduler, the
 *    on-line adjustment of the scheduling priorities to cope with
 *    interactive/non interactive threads discrimination is not
 *    supported: threads keep having the same priority as long as the
 *    user does not change it.
 *
 * The functions below manage CPU queues, and are NEVER responsible
 * for context switches (see thread.h for that) or synchronizations
 * (see kwaitq.h or the higher levels primitives [mutex, semaphore,
 * ...] for that).
 *
 * @note IMPORTANT: all the functions below are meant to be called
 * ONLY by the thread/timer/kwaitq subsystems. DO NOT use them
 * directly from anywhere else: use ONLY the thread/kwaitq functions!
 * If you still want to call them directly despite this disclaimer,
 * simply disable interrupts before clling them.
 */

#include <sos/errno.h>
#include <sos/time.h>


/**
 * The definition of a priority
 */
typedef unsigned char sos_sched_priority_t;


#include <sos/thread.h>


/**
 * Valid priority interval ("real-time" and non real-time threads altogether)
 */
#define SOS_SCHED_PRIO_HIGHEST 0
#define SOS_SCHED_PRIO_LOWEST  63
#define SOS_SCHED_NUM_PRIO     64


/**
 * Class-specific priorities
 */
#define SOS_SCHED_PRIO_RT_HIGHEST 0  /**< Highest 'real-time' static prio. */
#define SOS_SCHED_PRIO_RT_LOWEST  15 /**< Lowest 'real-time' static priority */
#define SOS_SCHED_PRIO_TS_HIGHEST 16 /**< Highest time-sharing priority */
#define SOS_SCHED_PRIO_TS_LOWEST  63 /**< Lowest time-sharing priority */

#define SOS_SCHED_PRIO_DEFAULT 40    /**< Default priority */


/**
 * Helper macros (Yes, priorities ordered in decreasing numerical value)
 *
 * @note: The use of this function is RESERVED
 */
#define SOS_SCHED_PRIO_CMP(prio1,prio2)  ((prio1) - (prio2))

#define SOS_SCHED_PRIO_IS_VALID(prio) \
  ({ int __prio = (int)(prio); \
     ((__prio) <= SOS_SCHED_PRIO_LOWEST) \
      && \
     ((__prio) >= SOS_SCHED_PRIO_HIGHEST); })

#define SOS_SCHED_PRIO_IS_RT(prio)    \
  ({ int __prio = (int)(prio); \
     ((__prio) <= SOS_SCHED_PRIO_RT_LOWEST) \
      && \
     ((__prio) >= SOS_SCHED_PRIO_RT_HIGHEST); })


/**
 * The time slices for 'time-sharing' user threads, in ms
 */
#define SOS_TIME_SLICE_MIN  10 /* for SOS_SCHED_PRIO_TS_HIGHEST */
#define SOS_TIME_SLICE_MAX 200 /* for SOS_SCHED_PRIO_TS_LOWEST */


/**
 * Initialize the scheduler
 *
 * @note: The use of this function is RESERVED
 */
sos_ret_t sos_sched_subsystem_setup(void);


/**
 * Mark the given thread as ready
 *
 * @note: The use of this function is RESERVED
 */
sos_ret_t sos_sched_set_ready(struct sos_thread * thr);


/**
 * Return the identifier of the next thread to run. Also removes it
 * from the ready list, but does NOT set is as current_thread !
 *
 * @param current_thread TCB of the thread calling the function
 *
 * @param do_yield When TRUE, put the current executing thread at the
 * end of the ready list. Otherwise it is kept at the head of it.
 *
 * @note: The use of this function is RESERVED
 */
struct sos_thread * sos_reschedule(struct sos_thread * current_thread,
				    sos_bool_t do_yield);

/**
 * Called by thread subsystem each time a READY thread's priority is
 * changed
 *
 * @note: The use of this function is RESERVED (to thread.c)
 */
sos_ret_t sos_sched_change_priority(struct sos_thread * thr,
				    sos_sched_priority_t priority);


/**
 * Account for the execution of a time tick. Should be called
 * immediately before the timer ISR calls sos_reschedule() before
 * returning to thread context.
 *
 * @note The use of this function is RESERVED (to timer IRQ)
 */
sos_ret_t sos_sched_do_timer_tick(void);

#endif /* _SOS_WAITQUEUE_H_ */
