/* Copyright (C) 2004  The SOS Team

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_TYPES_H_
#define _SOS_TYPES_H_

/**
 * @file types.h
 *
 * SOS basic types definition
 */

/** Physical address */
typedef unsigned int           sos_paddr_t;

/** Kernel virtual address */
typedef unsigned int           sos_vaddr_t;

/** User virtual address */
typedef unsigned int           sos_uaddr_t;

/** Memory size of an object (positive) */
typedef unsigned int           sos_size_t;
/** Generic positive offset into objects */
typedef unsigned int           sos_uoffset_t;
/** Generic signed offset into objects */
typedef signed int             sos_soffset_t;
/** Generic positive LONG (64 bits) offset into objects */
typedef unsigned long long int sos_luoffset_t;
/** Generic signed LONG (64 bits) offset into objects */
typedef signed long long int   sos_lsoffset_t;

/** Generic count of objects */
typedef unsigned int           sos_count_t;
/** Generic count of objects (LARGE) */
typedef unsigned long long int sos_lcount_t;

/** Process identifiers */
typedef unsigned int           sos_pid_t;

/* Low-level sizes */
typedef unsigned long long int sos_ui64_t; /**< 32b unsigned */
typedef unsigned long int      sos_ui32_t; /**< 32b unsigned */
typedef unsigned short int     sos_ui16_t; /**< 16b unsigned */
typedef unsigned char          sos_ui8_t;  /**< 8b unsigned */

typedef signed long long int   sos_si64_t; /**< 64b signed */
typedef signed long int        sos_si32_t; /**< 32b signed */
typedef signed short int       sos_si16_t; /**< 16b signed */
typedef signed char            sos_si8_t;  /**< 8b signed */

typedef enum { FALSE=0, TRUE } sos_bool_t;

/** Not a proper type, but highly useful with basic type
    manipulations */
#define NULL ((void*)0)

#endif /* _SOS_TYPES_H_ */
