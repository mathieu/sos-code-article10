/* Copyright (C) 2004 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_CPULOAD_H_
#define _SOS_CPULOAD_H_

#include <sos/errno.h>
#include <sos/types.h>
#include <sos/time.h>


/**
 * @file calcload.h
 *
 * Management of the CPU load in the system. For three intervals
 * (1min, 5min, 15min), we maintain the user/kernel loads (ie number
 * of threads in user/kernel mode ready or running) and the
 * user/kernel CPU occupation ratio.
 */


/**
 * Reinitialize the calcload subsystem. Must be called after the time
 * subsystem has been initialized
 */
sos_ret_t sos_load_subsystem_setup(void);


/**
 * Get the current USER load for each of the intervals.  Definition:
 * the USER load is the mean number of threads in USER mode which are
 * ready or running over the period.
 *
 * @return the current USER load * SOS_LOAD_DISPLAY_MULTIPLICATION_FACTOR
 */
void sos_load_get_uload(sos_ui32_t * load_1mn,
			sos_ui32_t * load_5mn,
			sos_ui32_t * load_15mn);


/**
 * Get the current KERNEL load for each of the intervals.  Definition:
 * the KERNEL load is the mean number of threads in KERNEL mode which are
 * ready or running over the period.
 *
 * @note The load of the IDLE thread is removed from this computation !
 *
 * @return the current KERNEL load * SOS_LOAD_DISPLAY_MULTIPLICATION_FACTOR
 */
void sos_load_get_sload(sos_ui32_t * load_1mn,
			sos_ui32_t * load_5mn,
			sos_ui32_t * load_15mn);


/**
 * Get the current User CPU occupation ratio
 *
 * @return the current User/Kernel CPU occupation ratio
 *                     * SOS_LOAD_DISPLAY_MULTIPLICATION_FACTOR
 */
void sos_load_get_uratio(sos_ui32_t * load_1mn,
			  sos_ui32_t * load_5mn,
			  sos_ui32_t * load_15mn);


/**
 * Get the current Kernel CPU occupation ratio
 *
 * @note The load of the IDLE thread is NOT removed from this computation !
 *
 * @return the current User/Kernel CPU occupation ratio
 *                     * SOS_LOAD_DISPLAY_MULTIPLICATION_FACTOR
 */
void sos_load_get_sratio(sos_ui32_t * load_1mn,
			  sos_ui32_t * load_5mn,
			  sos_ui32_t * load_15mn);


/**
 * Generate the "dest" string with the string corresponding to
 * load_value / SOS_LOAD_DISPLAY_MULTIPLICATION_FACTOR with decimal
 * digits
 */
void sos_load_to_string(char dest[11], sos_ui32_t load_value);


/* ******************************************************
 * Restricted function. Used only by sched.c/time.c
 */


/**
 * Restricted callback called from the scheduler subsystem to update
 * the load parameters.
 *
 * @note This is RESTRICTED function to be used by time.c
 */
sos_ret_t sos_load_do_timer_tick(sos_bool_t cur_is_user,
				 sos_ui32_t nb_user_ready,
				 sos_ui32_t nb_kernel_ready);


#endif /* _SOS_CPULOAD_H_ */
