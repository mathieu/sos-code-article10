/* Copyright (C) 2005,2006      David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
*/
#ifndef _SOS_BLKCACHE_H_
#define _SOS_BLKCACHE_H_


/**
 * @file blkcache.h
 *
 * Simple block cache interface. This implementation is based on a
 * fixed-size block cache, the size being set at construction time.
 */
#include <sos/errno.h>
#include <sos/blkdev.h>


/** Opaque structure holding a cache */
struct sos_block_cache;

/** Opaque structure holding a cache entry (used as a cookie from
    blkcache_retrieve to blkcache_release) */
struct sos_block_cache_entry;


sos_ret_t sos_blkcache_subsystem_setup(void);


/** Create a new block cache */
struct sos_block_cache *
sos_blkcache_new_cache(void * blockdev_instance_custom_data,
		       sos_size_t  block_size,
		       sos_count_t cache_size_in_blocks,
		       struct sos_blockdev_operations * blockdev_ops);


/** Delete a block cache */
sos_ret_t
sos_blkcache_delete_cache(struct sos_block_cache * bc);


/**
 * "Write-only" cached blocks are expected to be *completely*
 * overwritten by the blkdev code
 */
typedef enum { SOS_BLKCACHE_READ_ONLY  = 0x4242,
	       SOS_BLKCACHE_READ_WRITE = 0x2442,
	       SOS_BLKCACHE_WRITE_ONLY = 0x4224 } sos_blkcache_access_type_t;


/**
 * Retrieve a block from the cache
 *  @param block_contents is filled with the address of the kernel
 *    buffer holding the data
 *  @return opaque structure used by release_block, or NULL when the
 *    block could not be retrieved from disk
 *
 * @note once used and/or modified, the block must be released.
 */
struct sos_block_cache_entry *
sos_blkcache_retrieve_block(struct sos_block_cache * bc,
			    sos_luoffset_t block_index,
			    sos_blkcache_access_type_t access_type,
			    sos_vaddr_t */*out*/ block_contents);


/**
 * Unreference a block previously retrieved. If is_dirty is TRUE, the
 * block MUST have been retrieved with the "read_write" or
 * "write_only" access type.
 */
sos_ret_t
sos_blkcache_release_block(struct sos_block_cache * bc,
			   struct sos_block_cache_entry * entry,
			   sos_bool_t is_dirty,
			   sos_bool_t force_flush);


/** Flush any modified blocks to disk */
sos_ret_t
sos_blkcache_flush(struct sos_block_cache * bc);

#endif
