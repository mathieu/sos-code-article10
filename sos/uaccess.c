/* Copyright (C) 2005  David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/

#include <hwcore/paging.h>
#include <sos/thread.h>
#include <sos/assert.h>
#include <sos/kmalloc.h>

#include "uaccess.h"


int __uaccess_zero_fool_gcc = 0;
/**
 * Make sure gcc optimizations don't go too far and don't suppress the
 * code at the given label because 1/ a global return is "above" (wrt
 * source code lines), 2/ it is never "goto"
 */
#define KEEP_LABEL(lbl) \
 ({ if (__uaccess_zero_fool_gcc) goto lbl; })


/**
 * @param user_as The user address space we want to transfer from/to
 *
 * @return >=0 : number of bytes successfully transferred, <0 an error code
 */
static sos_ret_t nocheck_user_memcpy(struct sos_umem_vmm_as * user_as,
				     sos_vaddr_t dest,
				     sos_vaddr_t src,
				     sos_size_t size,
				     sos_bool_t transfer_from_user)
{
  __label__ catch_pgflt;
  char *cptr_dst;
  const char *cptr_src;
  sos_size_t transfer_sz;
  sos_ret_t retval;

  KEEP_LABEL(catch_pgflt);

  if (size <= 0)
    return 0;

  retval = sos_thread_prepare_user_space_access(user_as,
						(sos_vaddr_t) && catch_pgflt);
  if (SOS_OK != retval)
    return retval;

  /* Version of memcpy that does not alter the stack pointer, in
     order to be able to abruptedly jump to catch_pgflt without stack
     inconsistency. That's the reason why we don't call the normal
     memcpy here: on page fault, it would return back in here, with
     the stack frame returning back here again */
  for (cptr_dst = (char*)dest,
	 cptr_src = (const char*)src,
	 transfer_sz = size ;
       transfer_sz > 0 ;
       cptr_dst++, cptr_src++, transfer_sz--)
    *cptr_dst = *cptr_src;

  retval = sos_thread_end_user_space_access();
  if (SOS_OK != retval)
    return retval;

  return size;

  /* An unresolved page fault occured while accessing user space */
 catch_pgflt:
  {
    struct sos_thread * cur_thr = sos_thread_get_current();
    sos_uaddr_t faulted_uaddr = cur_thr->fixup_uaccess.faulted_uaddr;

    if (transfer_from_user)
      {
	if ( (faulted_uaddr < src) || (faulted_uaddr - size > src) )
	  sos_display_fatal_error("Unexpected read access in user space");
	retval = faulted_uaddr - src;
      }
    else
      {
	if ( (faulted_uaddr < dest) || (faulted_uaddr - size > dest) )
	  sos_display_fatal_error("Unexpected write access in user space");
	retval = faulted_uaddr - dest;
      }

    sos_thread_end_user_space_access();
    return retval;
  }
}


sos_ret_t sos_memcpy_from_user(sos_vaddr_t kernel_to,
			       sos_uaddr_t user_from,
			       sos_size_t size)
{
  /* Make sure user is trying to access user space */
  if (! SOS_PAGING_IS_USER_AREA(user_from, size) )
    return -SOS_EPERM;

  /* Make sure the copy totally resides inside kernel space */
  if (! SOS_PAGING_IS_KERNEL_AREA(kernel_to, size) )
    return -SOS_EPERM;

  return nocheck_user_memcpy(NULL, kernel_to, user_from, size, TRUE);
}


sos_ret_t sos_memdup_from_user(sos_vaddr_t * kernel_to, sos_uaddr_t from_user,
			       sos_size_t length,
			       sos_ui32_t kmalloc_flags)
{
  sos_ret_t retval;

  if (length <= 0)
    return -SOS_EINVAL;

  *kernel_to = sos_kmalloc(length, kmalloc_flags);
  if (NULL == (void*) *kernel_to)
    return -SOS_ENOMEM;

  retval = sos_memcpy_from_user(*kernel_to, from_user, length);
  if ((sos_ret_t)length != retval)
    {
      sos_kfree((sos_vaddr_t)*kernel_to);
      *kernel_to = (sos_vaddr_t) NULL;
      retval = -SOS_EFAULT;
    }
  else
    retval = SOS_OK;

  return retval;
}


sos_ret_t sos_memcpy_to_user(sos_uaddr_t user_to,
			     sos_vaddr_t kernel_from,
			     sos_size_t size)
{
  /* Make sure the source totally resides inside kernel space */
  if (! SOS_PAGING_IS_KERNEL_AREA(kernel_from, size) )
    return -SOS_EPERM;

  /* Make sure user is trying to access user space */
  if (! SOS_PAGING_IS_USER_AREA(user_to, size) )
    return -SOS_EPERM;

  return nocheck_user_memcpy(NULL, user_to, kernel_from, size, FALSE);
}


sos_ret_t sos_memcpy_from_specified_userspace(sos_vaddr_t kernel_to,
					      struct sos_umem_vmm_as * src_as,
					      sos_uaddr_t user_from,
					      sos_size_t size)
{
  if (NULL == src_as)
    return -SOS_EINVAL;
  
  /* Make sure user is trying to access user space */
  if (! SOS_PAGING_IS_USER_AREA(user_from, size) )
    return -SOS_EPERM;

  /* Make sure the copy totally resides inside kernel space */
  if (! SOS_PAGING_IS_KERNEL_AREA(kernel_to, size) )
    return -SOS_EPERM;

  return nocheck_user_memcpy(src_as, kernel_to, user_from, size, TRUE);
}


sos_ret_t sos_memcpy_to_specified_userspace(struct sos_umem_vmm_as * dst_as,
					    sos_uaddr_t user_to,
					    sos_vaddr_t kernel_from,
					    sos_size_t size)
{
  if (NULL == dst_as)
    return -SOS_EINVAL;
  
  /* Make sure the source totally resides inside kernel space */
  if (! SOS_PAGING_IS_KERNEL_AREA(kernel_from, size) )
    return -SOS_EPERM;

  /* Make sure user is trying to access user space */
  if (! SOS_PAGING_IS_USER_AREA(user_to, size) )
    return -SOS_EPERM;

  return nocheck_user_memcpy(dst_as, user_to, kernel_from, size, FALSE);
}


sos_ret_t sos_usercpy(struct sos_umem_vmm_as * dst_as,
		      sos_uaddr_t dst_uaddr,
		      struct sos_umem_vmm_as * src_as,
		      sos_uaddr_t src_uaddr,
		      sos_size_t size)
{
  sos_vaddr_t kern_addr;
  sos_ret_t retval;

  if (size <= 0)
    return 0;

  /* Make sure user is trying to access user space */
  if (! SOS_PAGING_IS_USER_AREA(src_uaddr, size) )
    return -SOS_EPERM;
  if (! SOS_PAGING_IS_USER_AREA(dst_uaddr, size) )
    return -SOS_EPERM;

  kern_addr = sos_kmalloc(size, 0);
  if (! kern_addr)
    return -SOS_ENOMEM;

  retval = nocheck_user_memcpy(src_as, kern_addr, src_uaddr,
			       size, TRUE);
  if (retval <= 0)
    {
      sos_kfree((sos_vaddr_t)kern_addr);
      return retval;
    }

  retval = nocheck_user_memcpy(dst_as, dst_uaddr, kern_addr,
			       retval, FALSE);

  sos_kfree((sos_vaddr_t)kern_addr);

  return retval;
}


sos_ret_t sos_strnlen_from_user(sos_uaddr_t user_str, sos_size_t max_len)
{
  __label__ catch_pgflt;
  const char *sc;
  sos_ret_t retval;

  KEEP_LABEL(catch_pgflt);

  if (max_len <= 0)
    return 0;

  /* Make sure user is trying to access user space */
  if (! SOS_PAGING_IS_USER_AREA(user_str, max_len) )
    return -SOS_EPERM;

  retval = sos_thread_prepare_user_space_access(NULL,
						(sos_vaddr_t) && catch_pgflt);
  if (SOS_OK != retval)
    return retval;

  /* Version of strnlen that does not alter the stack pointer, in
     order to be able to abruptedly jump to catch_pgflt without stack
     inconsistency. That's the reason why we don't call the normal
     strnlen here: on page fault, it would return back in here, with
     the stack frame returning back here again */
  for (sc = (const char *)user_str; max_len-- && *sc != '\0'; ++sc)
    continue;


  retval = sos_thread_end_user_space_access();
  if (SOS_OK != retval)
    return retval;

  return ((sos_uaddr_t)sc - user_str);

  /* An unresolved page fault occured while accessing user space */
 catch_pgflt:
  {
    sos_thread_end_user_space_access();
    return -SOS_EFAULT;
  }
}


static sos_ret_t nocheck_user_strzcpy(char *dst, const char *src,
				      sos_size_t len)
{
  __label__ catch_pgflt;
  unsigned int i;
  sos_ret_t retval;

  KEEP_LABEL(catch_pgflt);

  if (len <= 0)
    return 0;

  retval = sos_thread_prepare_user_space_access(NULL,
						(sos_vaddr_t) && catch_pgflt);
  if (SOS_OK != retval)
    return retval;

  /* Version of strzcpy that does not alter the stack pointer, in
     order to be able to abruptedly jump to catch_pgflt without stack
     inconsistency. That's the reason why we don't call the normal
     strzcpy here: on page fault, it would return back in here, with
     the stack frame returning back here again */
  for (i = 0; i < len; i++)
    {
      dst[i] = src[i];
      if(src[i] == '\0')
        break;
    }
  
  dst[len-1] = '\0'; 

  retval = sos_thread_end_user_space_access();
  if (SOS_OK != retval)
    return retval;

  return SOS_OK;

  /* An unresolved page fault occured while accessing user space */
 catch_pgflt:
  {
    sos_thread_end_user_space_access();
    return -SOS_EFAULT;
  }
}


sos_ret_t sos_strzcpy_from_user(char *kernel_to, sos_uaddr_t user_from,
				sos_size_t max_len)
{
  if (max_len <= 0)
    return -SOS_EINVAL;

  /* Make sure user is trying to access user space */
  if (! SOS_PAGING_IS_USER_AREA(user_from, max_len) )
    return -SOS_EPERM;

  /* Make sure the copy totally resides inside kernel space */
  if (! SOS_PAGING_IS_KERNEL_AREA((sos_vaddr_t)kernel_to, max_len) )
    return -SOS_EPERM;

  return nocheck_user_strzcpy(kernel_to, (const char*)user_from, max_len);
}


sos_ret_t sos_strzcpy_to_user(sos_uaddr_t user_to, const char *kernel_from,
			      sos_size_t max_len)
{
  if (max_len <= 0)
    return -SOS_EINVAL;

  /* Make sure the source totally resides inside kernel space */
  if (! SOS_PAGING_IS_KERNEL_AREA((sos_vaddr_t)kernel_from, max_len) )
    return -SOS_EPERM;

  /* Make sure user is trying to access user space */
  if (! SOS_PAGING_IS_USER_AREA(user_to, max_len) )
    return -SOS_EPERM;

  return nocheck_user_strzcpy((char*)user_to, kernel_from, max_len);
}


sos_ret_t sos_strndup_from_user(char ** kernel_to, sos_uaddr_t from_user,
				sos_size_t max_len,
				sos_ui32_t kmalloc_flags)
{
  sos_ret_t retval = sos_strnlen_from_user(from_user, max_len);
  if (retval < 0)
    return retval;

  *kernel_to = (char*)sos_kmalloc(retval + 1, kmalloc_flags);
  if (NULL == *kernel_to)
    return -SOS_ENOMEM;

  retval = sos_strzcpy_from_user(*kernel_to, from_user, retval + 1);
  if (SOS_OK != retval)
    {
      sos_kfree((sos_vaddr_t)*kernel_to);
      *kernel_to = NULL;
    }

  return retval;
}


sos_ret_t sos_memcpy_generic_to(sos_genaddr_t to_addr,
				sos_vaddr_t kernel_from,
				sos_size_t size)
{
  if (to_addr.is_user)
    return sos_memcpy_to_user(to_addr.addr, kernel_from, size);
  memcpy((void*)to_addr.addr, (const void*)kernel_from, size);
  return size;
}


sos_ret_t sos_memcpy_generic_from(sos_vaddr_t kernel_from,
				  sos_genaddr_t from_addr,
				  sos_size_t size)
{
  if (from_addr.is_user)
    return sos_memcpy_from_user(kernel_from, from_addr.addr, size);
  memcpy((void*)kernel_from, (const void*)from_addr.addr, size);
  return size;
}
