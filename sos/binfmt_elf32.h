/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_BINFMT_ELF32_H_
#define _SOS_BINFMT_ELF32_H_

/**
 * @file binfmt_elf32.h
 *
 * Userspace mapper to load ELF32 programs. For the 7.x articles, the
 * user programs are resident inside the kernel image, so that loading
 * them in userspace consists in remapping them in userspace.
 */

#include <sos/umem_vmm.h>

/**
 * Map a user ELF32 program into user memory.  Make sure the program
 * is in a valid ELF format, map it into memory, and return the
 * address of its entry point (ie _start function)
 *
 * @return 0 when the program is not a valid ELF. Return the start
 * address of the program otherwise
 */
sos_uaddr_t sos_binfmt_elf32_map(struct sos_umem_vmm_as * dest_as,
				 const char * progname);

#endif /* _SOS_BINFMT_ELF32_H_ */
