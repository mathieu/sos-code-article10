/* Copyright (C) 2005  David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_SYSCALL_H_
#define _SOS_SYSCALL_H_

/**
 * @file syscall.h
 *
 * SOS Syscalls identifiers and handler. The handler is called by the
 * underlying assembler routine (see hwcore/syscall.h)
 */


/**
 * The user services offered by the SOS kernel
 */
#define SOS_SYSCALL_ID_EXIT        67  /**< Args: retval, retval=NONE */


/**
 * Basic low-level user thread API
 */
#define SOS_SYSCALL_ID_NEW_THREAD  128 /**< Args: start_func start_arg1 start_arg2 stack_size, retval=errno */
#define SOS_SYSCALL_ID_NANOSLEEP   129 /**< Args: sec nanosec, retval=errno */


/**
 * User memory mappings management
 */
#define SOS_SYSCALL_ID_GETPID      256 /**< Args: NONE, retval=cur. proc. PID */
#define SOS_SYSCALL_ID_FORK        257 /**< Args: NONE, retval={father:child_pd, child:0} */
#define SOS_SYSCALL_ID_EXEC        258 /**< Args: ptr_exec_path strlen_exec_path addr_args len_args, retval=errno */
#define SOS_SYSCALL_ID_MUNMAP      260 /**< Args: start_uaddr size, retval=errno */
#define SOS_SYSCALL_ID_MPROTECT    261 /**< Args: start_uaddr size access_rights, retval=errno */
#define SOS_SYSCALL_ID_MRESIZE     262 /**< Args: old_uaddr old_size ptr_new_uaddr new_size flags, retval=errno */
#define SOS_SYSCALL_ID_MSYNC       263 /**< Args: start_uaddr size flags, retval=errno */

/**
 * Heap management
 */
#define SOS_SYSCALL_ID_BRK         303 /**< Args: 0/new_top_heap, retval=top_heap */

/**
 * File system interface
 */
#define SOS_SYSCALL_ID_MOUNT       555 /**< Args: uaddr_src srclen uaddr_dst dstlen uaddr_fstype flags uaddr_args, retval=errno */
#define SOS_SYSCALL_ID_UMOUNT      556 /**< Args: uaddr_path pathlen, retval=errno */
#define SOS_SYSCALL_ID_SYNC        557 /**< Args: none, retval=errno */
#define SOS_SYSCALL_ID_VFSTAT64    558 /**< Args: uaddr_path pathlen uaddr_vfstat_struct, retval=errno */

#define SOS_SYSCALL_ID_OPEN        559 /**< Args: path pathlen flags access_rights, retval=fd */
#define SOS_SYSCALL_ID_CLOSE       560 /**< Args: fd, retval=errno */
#define SOS_SYSCALL_ID_READ        561 /**< Args: fd uaddr_buf uaddr_buflen, retval=errno */
#define SOS_SYSCALL_ID_READDIR     562 /**< Args: fd uaddr_dirent, retval=errno */
#define SOS_SYSCALL_ID_WRITE       563 /**< Args: fd uaddr_buf uaddr_buflen, retval=errno */
#define SOS_SYSCALL_ID_SEEK64      564 /**< Args: fd uaddr_offset whence, retval=errno */
#define SOS_SYSCALL_ID_FTRUNCATE64 565 /**< Args: fd length, retval=errno */
#define SOS_SYSCALL_ID_FSMMAP      566 /**< Args: &hint len prot flags fd uoffs64_hi uoffs64_lo, retval=errno */
#define SOS_SYSCALL_ID_FSYNC       567 /**< Args: fd, retval=errno */
#define SOS_SYSCALL_ID_FCNTL       568 /**< Args: fd cmd arg, retval=errno */
/* fcntl possible commands */
#  define SOS_FCNTL_DUPFD          0x4201
#  define SOS_FCNTL_GETFD          0x4202
#  define SOS_FCNTL_SETFD          0x4203
#  define SOS_FCNTL_GETFL          0x4204
#  define SOS_FCNTL_SETFL          0x4205
#define SOS_SYSCALL_ID_IOCTL       569 /**< Args: fd cmd arg, retval=errno */

#define SOS_SYSCALL_ID_CREAT       570 /**< Args: uaddr_path pathlen access_rights, retval=errno */
#define SOS_SYSCALL_ID_LINK        571 /**< Args: uaddr_oldpath oldpathlen uaddr_newpath newpathlen, retval=errno */
#define SOS_SYSCALL_ID_RENAME      572 /**< Args: uaddr_oldpath oldpathlen uaddr_newpath newpathlen, retval=errno */
#define SOS_SYSCALL_ID_UNLINK      573 /**< Args: uaddr_path pathlen, retval=errno */
#define SOS_SYSCALL_ID_SYMLINK     574 /**< Args: uaddr_path pathlen uaddr_target targetlen, retval=errno */
#define SOS_SYSCALL_ID_MKNOD       575 /**< Args: uaddr_path pathlen type access_rights major minor, retval=errno */

#define SOS_SYSCALL_ID_MKDIR       576 /**< Args: uaddr_path pathlen access_rights, retval=errno */
#define SOS_SYSCALL_ID_RMDIR       577 /**< Args: uaddr_path pathlen, retval=errno */

#define SOS_SYSCALL_ID_CHMOD       578 /**< Args: uaddr_path pathlen access_rights, retval=errno */
#define SOS_SYSCALL_ID_STAT64      579 /**< Args: uaddr_path pathlen nofollow uaddr_stat_struct, retval=errno */

#define SOS_SYSCALL_ID_CHROOT      580 /**< Args: uaddr_path pathlen, retval=errno */
#define SOS_SYSCALL_ID_CHDIR       581 /**< Args: uaddr_path pathlen, retval=errno */
#define SOS_SYSCALL_ID_FCHDIR      582 /**< Args: fd, retval=errno */


#define SOS_SYSCALL_ID_BOCHS_WRITE 43  /**< Args: string, retval=num_printed */


#if defined(KERNEL_SOS) && !defined(ASM_SOURCE)

#include <sos/errno.h>
#include <hwcore/cpu_context.h>

/**
 * "The" SOS syscall handler
 *
 * @param syscall_id The identifier of the syscall service
 * @param user_ctxt The user context making the syscall
 */
sos_ret_t sos_do_syscall(int syscall_id,
			 const struct sos_cpu_state *user_ctxt);

#endif /* defined(KERNEL_SOS) && !defined(ASM_SOURCE) */

#endif /* _SOS_SYSCALL_H_ */
