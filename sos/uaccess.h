/* Copyright (C) 2005  David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_UACCESS_H_
#define _SOS_UACCESS_H_


/**
 * @file uaccess.h
 *
 * Routines to access user-space data from inside the kernel space.
 */

#include <sos/types.h>
#include <sos/errno.h>
#include <sos/umem_vmm.h>

/**
 * Retrieve a bunch of data from the user space of the
 * current_thread->process
 *
 * @return <0 on error ! Return the number of bytes successfully copied
 * otherwise.
 */
sos_ret_t sos_memcpy_from_user(sos_vaddr_t kernel_to,
			       sos_uaddr_t user_from,
			       sos_size_t size);


/**
 * Retrieve a bunch of data from the user space of the
 * current_thread->process and copy it in a newly allocated kernel
 * area
 *
 * @return NULL on error (including unresolved page fault during
 * transfer)
 */
sos_ret_t sos_memdup_from_user(sos_vaddr_t * kernel_to, sos_uaddr_t from_user,
			       sos_size_t length,
			       sos_ui32_t kmalloc_flags);


/**
 * Transfer a bunch of data from kernel space into the user space of
 * the current_thread->process
 *
 * @return <0 n error ! Return the number of bytes successfully copied
 * otherwise.
 */
sos_ret_t sos_memcpy_to_user(sos_uaddr_t user_to,
			     sos_vaddr_t kernel_from,
			     sos_size_t size);


/**
 * Variant of sos_memcpy_from_user()
 * Retrieve a bunch of data from the user space of the given src_as
 * address space into kernel space
 *
 * @return NULL on error (including unresolved page fault during
 * transfer)
 *
 * @note src_as MUST NOT be NULL
 */
sos_ret_t sos_memcpy_from_specified_userspace(sos_vaddr_t kernel_to,
					      struct sos_umem_vmm_as * src_as,
					      sos_uaddr_t user_from,
					      sos_size_t size);


/**
 * Variant of sos_memcpy_to_user()
 * Transfer a bunch of data from kernel space into the user space of
 * the given dst_as address space
 *
 * @return <0 n error ! Return the number of bytes successfully copied
 * otherwise.
 *
 * @note dst_as MUST NOT be NULL
 */
sos_ret_t sos_memcpy_to_specified_userspace(struct sos_umem_vmm_as * dst_as,
					    sos_uaddr_t user_to,
					    sos_vaddr_t kernel_from,
					    sos_size_t size);

/**
 * Copy data from an user space into another
 * @return The number of bytes successfuly copied
 * @note dst_as and src_as may be NULL, in which case the current
 * thread->process address space is used for the copy
 */
sos_ret_t sos_usercpy(struct sos_umem_vmm_as * dst_as,
		      sos_uaddr_t dst_uaddr,
		      struct sos_umem_vmm_as * src_as,
		      sos_uaddr_t src_uaddr,
		      sos_size_t size);


/**
 * @return the length of the given user space string user_str
 * (excluding the trailing \0), up to max_len bytes. <0 on error
 * (unresolved page fault, etc.)
 */
sos_ret_t sos_strnlen_from_user(sos_uaddr_t user_str, sos_size_t max_len);


/**
 * Copy the given user space string to kernel space, up to max_len
 * bytes (including trailing \0)
 *
 * @return SOS_OK on success, <0 otherwise (unresolved page fault, etc.)
 */
sos_ret_t sos_strzcpy_from_user(char *kernel_to, sos_uaddr_t user_from,
				sos_size_t max_len);


/**
 * Copy the given kernel string to user space, up to max_len bytes
 * (including trailing \0)
 *
 * @return SOS_OK on success, <0 otherwise (unresolved page fault, etc.)
 */
sos_ret_t sos_strzcpy_to_user(sos_uaddr_t user_to, const char *kernel_from,
			      sos_size_t max_len);


/**
 * Copy the given user space string into a new allocated kernel space
 * area of the correct size, up to max_len bytes (including trailing
 * \0)
 *
 * @return SOS_OK on success + *kernel_to is set to the freshly
 * allocated kernel string, <0 otherwise (unresolved page fault, etc.)
 */
sos_ret_t sos_strndup_from_user(char ** kernel_to, sos_uaddr_t from_user,
				sos_size_t max_len,
				sos_ui32_t kmalloc_flags);


/*
 * Special functions to access both kernel/user space
 */
/** Generic (Kernel or User) virtual address. We define a structure to
    force the compiler to signal when we call this special function
    with a usual address... */
typedef struct sos_genaddr { sos_bool_t is_user;
                             sos_ui32_t addr; } sos_genaddr_t;
#define SOS_GENADDR_DECL(name,_is_user,_addr) \
  sos_genaddr_t name = (struct sos_genaddr) { .is_user=_is_user, .addr=_addr }

sos_ret_t sos_memcpy_generic_to(sos_genaddr_t to_addr,
				sos_vaddr_t kernel_from,
				sos_size_t size);

sos_ret_t sos_memcpy_generic_from(sos_vaddr_t kernel_from,
				  sos_genaddr_t from_addr,
				  sos_size_t size);

#endif /* _SOS_UACCESS_H_ */
