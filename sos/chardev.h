/* Copyright (C) 2005      David Decotigny, Thomas Petazzoni

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
*/
#ifndef _SOS_CHARDEV_H_
#define _SOS_CHARDEV_H_

/**
 * @file chardev.h
 *
 * Interface between the VFS and the "character" devices. The
 * following functions provide the mechanisms to bind the "character
 * device" nodes (@see mknod) to their device driver.
 *
 * The VFS layer must be perceived as an uniform access library on top
 * of a set of specialized FS. The "chardev" layer is to be perceived
 * as a FS-agnostic layer on top of the FS that binds the special
 * "character device" nodes to a set of system-wide read/write/seek
 * functions.
 */

#include "fs.h"

/**
 * The fundamental callbacks for a character device: they are common
 * to all the character devices of the same class. One is free to do
 * whatever she likes with the "custom_data" of the opened file passed
 * as argument
 */
struct sos_chardev_ops {
  /**
   * @note also called upon a "duplicate_opened_file", aka upon a
   * fork()
   * @note When this callback is called, of is NOT bound to any
   * nscache_node, so don't ever call any sos_fs_nscache_* function !
   * @note To get the fsnode associated to "of", don't call
   * sos_fs_nscache_get_fs_node(): it is already given by the "fsnode"
   * argument
   * @note MANDATORY !
   */
  sos_ret_t (*open)(struct sos_fs_node        * fsnode,
		    struct sos_fs_opened_file * of,
		    void * chardev_class_custom_data);

  /**
   * Called each time an opened "character device" is declared unused
   * by user space
   * @note Optional (might be NULL)
   */
  sos_ret_t (*close)(struct sos_fs_opened_file * of,
		     void * chardev_class_custom_data);

  /**
   * @note Optional (might be NULL), may block. Appropriate locking
   * MUST be implemented
   */
  sos_ret_t (*seek)(struct sos_fs_opened_file *this,
		    sos_lsoffset_t offset,
		    sos_seek_whence_t whence,
		    /* out */ sos_lsoffset_t * result_position);

  /**
   * @note Optional (might be NULL), may block. Appropriate locking
   * MUST be implemented
   */
  sos_ret_t (*read)(struct sos_fs_opened_file *this,
		    sos_uaddr_t dest_buf,
		    sos_size_t * /* in/out */len);

  /**
   * @note Optional (might be NULL), may block. Appropriate locking
   * MUST be implemented
   * @note Please call sos_fs_mark_dirty() if disk contents is changed
   */
  sos_ret_t (*write)(struct sos_fs_opened_file *this,
		     sos_uaddr_t src_buf,
		     sos_size_t * /* in/out */len);

  /**
   * @note Optional (might be NULL), may block. Appropriate locking
   * MUST be implemented
   * @note Please call sos_fs_mark_dirty() if disk contents is changed
   */
  sos_ret_t (*mmap)(struct sos_fs_opened_file *this,
		    sos_uaddr_t *uaddr, sos_size_t size,
		    sos_ui32_t access_rights,
		    sos_ui32_t flags,
		    sos_luoffset_t offset);

  /**
   * @note Optional (might be NULL), may block. Appropriate locking
   * MUST be implemented
   * @note Please call sos_fs_mark_dirty() if disk contents is changed
   */
  sos_ret_t (*fcntl)(struct sos_fs_opened_file *this,
		     int req_id,
		     sos_ui32_t req_arg /* Usually: sos_uaddr_t */);

  /**
   * @note Optional (might be NULL), may block. Appropriate locking
   * MUST be implemented
   * @note Please call sos_fs_mark_dirty() if disk contents is changed
   */
  sos_ret_t (*ioctl)(struct sos_fs_opened_file *this,
		     int req_id,
		     sos_ui32_t req_arg /* Usually: sos_uaddr_t */);
};


/**
 * Associate the given set of operations to the given major number.
 *
 * @note Character device drivers are registered for a complete class
 * of character devices (up to 4 billion devices per class)
 */
sos_ret_t sos_chardev_register_class (sos_ui32_t device_class,
				      struct sos_chardev_ops * ops,
				      void * chardev_class_custom_data);


/**
 * Unbind the given set of operations with the given major number
 *
 * @return SOS_EBUSY when the character device is still opened by any
 * process.
 */
sos_ret_t sos_chardev_unregister_class (sos_ui32_t device_class);


/*
 * Callbacks restricted to fs.c internals
 */

/**
 * Update the FS node ops_blockdev callbacks after an FS
 * allocate_new_node or fetch_node_from_disk, in order to point to
 * the block layer API functions
 */
sos_ret_t sos_chardev_helper_ref_new_fsnode(struct sos_fs_node * this);
sos_ret_t sos_chardev_helper_release_fsnode(struct sos_fs_node * this);

#endif
