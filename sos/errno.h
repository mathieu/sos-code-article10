/* Copyright (C) 2004  The SOS Team

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_ERRNO_H_
#define _SOS_ERRNO_H_

/**
 * @file errno.h
 *
 * SOS return value codes and errors.
 */

/* Positive values of the error codes */
#define SOS_OK            0   /* No error */
#define SOS_EINVAL        1   /* Invalid argument */
#define SOS_ENOSUP        2   /* Operation not supported */
#define SOS_ENOMEM        3   /* No available memory */
#define SOS_EBUSY         4   /* Object or device still in use */
#define SOS_EINTR         5   /* Wait/Sleep has been interrupted */
#define SOS_EAGAIN        6   /* Temporary resource exhaustion */
#define SOS_EPERM         7   /* Mutex/files ownership error */
#define SOS_EFAULT        8   /* Unresolved virtual memory fault */
#define SOS_ENOENT        9   /* No such file or directory */
#define SOS_ELOOP        10   /* symlink resolution loop / too recursive */
#define SOS_EEXIST       11   /* File already exists */
#define SOS_EACCES       12   /* Permission denied */
#define SOS_ENOTDIR      13   /* Dir does not exist */
#define SOS_ENAMETOOLONG 14
#define SOS_EXDEV        15   /* Cannot link entries across different FS */
#define SOS_EISDIR       16   /* Directories not allowed in operation */
#define SOS_ENOTEMPTY    17
#define SOS_ENODEV       18   /* No such device */
#define SOS_EBADF        19   /* Bad file descriptor */
#define SOS_EMFILE       20   /* Reached maximal opened file for process */
#define SOS_ENOSYS       21   /* Operation not implemented */
#define SOS_EIO          22   /* Input/output error */
#define SOS_EFATAL      255 /* Internal fatal error */

/* A negative value means that an error occured.  For
 *  example -SOS_EINVAL means that the error was "invalid
 * argument" */
typedef int sos_ret_t;

#endif /* _SOS_ERRNO_H_ */
