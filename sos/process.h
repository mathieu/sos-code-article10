/* Copyright (C) 2005 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_PROCESS_H_
#define _SOS_PROCESS_H_

/**
 * @file process.h
 *
 * SOS Definition of a process and associated management API. A
 * process is basically the collection of all the resources requested
 * by a user program. The threads are one of these resources, the
 * mm_context is one other example of such resources.  In SOS, a
 * "process" is mainly a resource manager, a container. It does not
 * provide much advanced functionality apart from a reference counter.
 *
 * Only the user threads belong to a process. The kernel
 * threads don't because the resources they hold are held by the whole
 * kernel, thus by ALL the threads (kernel AND user): the notion of
 * "process" doesn't have any meaning for them because they don't own
 * any proper resource (apart from their stack).
 */

#include <sos/errno.h>

/**
 * The definition of an SOS process is opaque. @see process.c
 */
struct sos_process;

#include <sos/thread.h>
#include <sos/fs.h>


/**
 * Default size of a user stack (8 MB)
 */
#define SOS_DEFAULT_USER_STACK_SIZE (8 << 20)


/** Number of pages for the pid bitmap (1 page = 32k PIDs) */
#define SOS_PROCESS_PID_BITMAP_NPAGES  1


/**
 * Initialization of the process subsystem
 */
sos_ret_t sos_process_subsystem_setup(void);


/**
 * Initialize a new process and return a reference to it. This
 * means that:
 *  - A new mm_context has been initialized
 *  - No threads belong to this process yet
 *  - If do_copy_current_process is FALSE:
 *     - Nothing is mapped in user space
 *     - No other resource is used
 *  - If do_copy_current_process is TRUE:
 *     - Same user mapping as in current thread's process
 *     - Same other resources used as for current thread's processs
 *
 * @return NULL on error (not enough memory)
 */
struct sos_process *sos_process_create(const char *name,
				       sos_bool_t do_copy_current_process);


/**
 * Signal we're using another reference to a process.
 */
sos_ret_t sos_process_ref(struct sos_process *proc);


/**
 * Release the reference to a process. If nobody holds a reference to
 * it and if the process does not have any thread anymore, the process
 * is destroyed.
 *
 * @return -SOS_EBUSY when the process is still referenced afterwards
 */
sos_ret_t sos_process_unref(struct sos_process *proc);


/**
 * Return the PID of the process
 */
sos_pid_t sos_process_get_pid(const struct sos_process *proc);


/**
 * Return the number of threads currently registered in the process
 */
sos_count_t sos_process_get_nb_threads(const struct sos_process *proc);


/**
 * Retrieve the address of the MMU configuration description
 *
 * @return NULL on error
 */
struct sos_mm_context *
sos_process_get_mm_context(const struct sos_process *proc);


/**
 * Retrieve the address space for the process
 *
 * @return NULL on error
 */
struct sos_umem_vmm_as *
sos_process_get_address_space(const struct sos_process *proc);


/**
 * Retrieve the root FS node of the process
 *
 * @return NULL on error
 */
struct sos_fs_opened_file *
sos_process_get_root(const struct sos_process *proc);


/**
 * Retrieve the current working dir of the process
 *
 * @return NULL on error
 */
struct sos_fs_opened_file *
sos_process_get_cwd(const struct sos_process *proc);


/**
 * Retrieve the opened file structure corresponding to the given FD
 *
 * @return NULL on error
 */
struct sos_fs_opened_file *
sos_process_get_opened_file(const struct sos_process *proc,
			    int fd);


/**
 * Change the root directory for the process
 */
sos_ret_t
sos_process_chroot(struct sos_process *proc,
		   struct sos_fs_opened_file * new_root,
		   struct sos_fs_opened_file ** old_root);


/**
 * Change the working directory of the process
 */
sos_ret_t
sos_process_chdir(struct sos_process *proc,
		  struct sos_fs_opened_file * new_cwd,
		  struct sos_fs_opened_file ** old_cwd);


/**
 * Allocate a new file descriptor for file
 *
 * @return >=0 on success, <0 on error (errno)
 */
sos_ret_t
sos_process_register_opened_file(struct sos_process *proc,
				 struct sos_fs_opened_file * of,
				 int start_search_at_fd);


/**
 * Free the given file descriptor
 *
 * @return >=0 on success, <0 on error (errno)
 */
sos_ret_t
sos_process_unregister_opened_file(struct sos_process *proc,
				   int fd);


/* ***************************************************
 * Restricted functions
 */


/**
 * Change the name of the process
 */
sos_ret_t sos_process_set_name(struct sos_process * proc,
			       const char * new_name);


/**
 * Attach the given thread to the process
 *
 * @note This function is called internally by thread.c . The thread
 * is assumed to be in the "CREATED" state, ie one cannot attach a
 * thread to some other process once it has been created.
 */
sos_ret_t sos_process_register_thread(struct sos_process *in_proc,
				      struct sos_thread *thr);


/**
 * Remove the given thread from the given process threads' list. When
 * the process becomes empty (ie does not contain any thread anymore
 * and is not referenced by anybody), all its resources are released.
 *
 * @note This function is called internally by thread.c . The thread
 * is assumed to be in the "ZOMBIE" state.
 */
sos_ret_t sos_process_unregister_thread(struct sos_thread *thr);


/**
 * Replace the address space of the process by another one
 *
 * @note The previous address space (if any) is deleted !
 */
sos_ret_t sos_process_set_address_space(struct sos_process *proc,
					struct sos_umem_vmm_as *new_as);

#endif /* _SOS_PROCESS_H_ */
