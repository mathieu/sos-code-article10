/* Copyright (C) 2005  David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_MMCTXT_H_
#define _SOS_MMCTXT_H_


/**
 * @file mm_context.h
 *
 * Low level API to manage multiple MMU translation tables. The API
 * (not its implementation) should be some kind of
 * architecture-independent.
 *
 * The goal of this API is:
 *   - To provide a simple arch-independent API to change the current
 *     address space configured in the MMU
 *   - To make sure that ALL the kernel space mappings are always kept
 *     IDENTICAL among all the address spaces in the whole system. That
 *     way, a virtual address in the kernel space refers to EXACTLY the
 *     same physical memory location for all the address spaces.
 */

#include <sos/types.h>
#include <sos/errno.h>


/**
 * Declaration of an MMU context. Opaque structure defined in
 * mm_context.c
 */
struct sos_mm_context;


/**
 * Setup the MMU context management subsystem
 */
sos_ret_t sos_mm_context_subsystem_setup(void);


/**
 * Public function to allocate a new MMU context.
 *
 * Allocate a new PD, map it into kernel virtual address space and
 * initialize its PDE for the Kernel space so that the Kernel space is
 * kept identical between ALL the MMU context in the system.
 */
struct sos_mm_context * sos_mm_context_create(void);


/**
 * Public function to create a new MMU context, copy of another one.
 *
 * Allocate a new PD, map it into kernel virtual address space,
 * initialize its PDE for the Kernel space so that the Kernel space is
 * kept identical between ALL the MMU context in the system and copy
 * the PDE/PT of the user space to match that of the model mm_context.
 */
struct sos_mm_context *
sos_mm_context_duplicate(const struct sos_mm_context *model);


/**
 * Public function to release the given MMU context (based on
 * reference counting). Only the virtual page that maps the PD of the
 * MMU context is released. All the other user-space pages are
 * expected to be already released by the parent process.
 */
sos_ret_t sos_mm_context_unref(struct sos_mm_context *mmctxt);


/**
 * Reconfigure the MMU to use a different MMU context. mmctxt MUST
 * NOT be NULL.
 */
sos_ret_t sos_mm_context_switch_to(struct sos_mm_context *mmctxt);


/* ******************************************************
 * Reserved functions
 */


/**
 * Reserved function to increase the reference count of the given MMU
 * context (based on reference counting).
 */
sos_ret_t sos_mm_context_ref(struct sos_mm_context *mmctxt);


/**
 * Restricted function reserved to paging.c to synchronize all the PDEs
 * accross all the MMU contexts in the system. This is limited to PDEs
 * pertaining to the kernel space.
 */
sos_ret_t sos_mm_context_synch_kernel_PDE(unsigned int index_in_pd,
					  sos_ui32_t pde);

#endif /* _SOS_MMCTXT_H_ */
