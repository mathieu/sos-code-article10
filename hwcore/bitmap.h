/* Copyright (C) 2006 David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_BITMAP_H_
#define _SOS_BITMAP_H_


/**
 * @file bitmap.h
 *
 * Bitmap manipulation. The implementation of this API is
 * architecture-dependent, hence its location in hwcore/
 */

#include <sos/types.h>
#include <sos/errno.h>


/**
 * Return index (start at start_bit_index) of first bit set
 * @return 0 < x <= bit_length when found, 0 when none found
 * @note in-memory size of area MUST be a multiple of 4B (32bits) !
 *
 * @note not atomic
 */
sos_size_t sos_bitmap_ffs(const void * area,
			  sos_size_t bit_length,
			  sos_size_t start_bit_index /* start at 1 */);


/**
 * Return index (start at start_bit_index) of first bit clear
 * @return 0 < x <= bit_length when found, 0 when none found
 * @note in-memory size of area MUST be a multiple of 4B (32bits) !
 *
 * @note not atomic
 */
sos_size_t sos_bitmap_ffc(const void * area,
			  sos_size_t bit_length,
			  sos_size_t start_bit_index /* start at 1 */);


/**
 * Set bit at index (start at 1)
 * @return old value of the bit
 *
 * @note atomic
 */
sos_bool_t
sos_bitmap_test_and_set(void * area,
			sos_size_t bit_index /* start at 1 */);


/**
 * Clear bit at index (start at 1)
 * @return old value of the bit
 *
 * @note atomic
 */
sos_bool_t
sos_bitmap_test_and_clear(void * area,
			  sos_size_t bit_index /* start at 1 */);


#endif /* _SOS_BITMAP_H_ */
