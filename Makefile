## Copyright (C) 2004,2005  The SOS Team
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
## USA. 

CC=gcc
LD=ld
CP=cp
STRIP=strip
CFLAGS  = -Wall -nostdinc -ffreestanding -DKERNEL_SOS -g -m32 -fno-asynchronous-unwind-tables
LIBGCC  = $(shell $(CC) -print-libgcc-file-name -m32) # To benefit from FP/64bits artihm.
LDFLAGS = --warn-common -nostdlib -m elf_i386
OBJECTS = bootstrap/multiboot.o					\
          hwcore/idt.o hwcore/gdt.o				\
	  hwcore/swintr.o hwcore/swintr_wrappers.o		\
	  hwcore/exception.o hwcore/exception_wrappers.o	\
	  hwcore/irq.o hwcore/irq_wrappers.o hwcore/i8259.o	\
	  hwcore/paging.o hwcore/i8254.o			\
	  hwcore/cpu_context.o hwcore/cpu_context_switch.o	\
	  hwcore/mm_context.o hwcore/bitmap.o			\
	  sos/kmem_vmm.o sos/kmem_slab.o sos/kmalloc.o		\
	  sos/physmem.o sos/klibc.o				\
	  sos/thread.o sos/kwaitq.o				\
          sos/time.o sos/sched.o sos/ksynch.o			\
	  sos/process.o sos/syscall.o				\
          sos/assert.o sos/main.o sos/mouse_sim.o		\
	  sos/uaccess.o sos/calcload.o				\
	  sos/umem_vmm.o sos/binfmt_elf32.o			\
	  drivers/x86_videomem.o drivers/bochs.o		\
	  drivers/zero.o drivers/mem.o				\
	  drivers/ide.o drivers/kbd.o drivers/kbdmapfr.o	\
	  drivers/serial.o drivers/tty.o drivers/part.o		\
	  drivers/console.o					\
	  sos/hash.o sos/fs.o sos/fs_nscache.o			\
	  drivers/fs_virtfs.o drivers/fs_fat.o 			\
	  sos/chardev.o sos/blkdev.o				\
	  sos/blkcache.o sos/fs_pagecache.o			\
          userland/userprogs.kimg

KERNEL_OBJ   = sos.elf
KERNEL_LOAD  = sos.gz
MULTIBOOT_IMAGE = fd.img
PWD := $(shell pwd | sed 's/"/\\\"/g;s/\$$/\\\$$/g')

# Main target
all: $(MULTIBOOT_IMAGE)

$(MULTIBOOT_IMAGE): $(KERNEL_LOAD)
	./support/build_image.sh $@ $< ""

$(KERNEL_LOAD): $(KERNEL_OBJ)
	$(CP) $< $<.strip && $(STRIP) -sx -R .comment $<.strip
	gzip < $<.strip > $@

$(KERNEL_OBJ): $(OBJECTS) ./support/sos.lds
	$(LD) $(LDFLAGS) -T ./support/sos.lds -o $@ $(OBJECTS) $(LIBGCC)
	-nm -C $@ | cut -d ' ' -f 1,3 > sos.map
	size $@

-include .mkvars

# Create the userland programs to include in the kernel image
userland/userprogs.kimg: FORCE
	$(MAKE) -C userland

# Create objects from C source code
%.o: %.c
	$(CC) "-I$(PWD)" -c "$<" $(CFLAGS) -o "$@"

# Create objects from assembler (.S) source code
%.o: %.S
	$(CC) "-I$(PWD)" -c "$<" $(CFLAGS) -DASM_SOURCE=1 -o "$@"

FORCE:
	@

# Clean directory
clean:
	$(RM) fd*.img *.o mtoolsrc *~ menu.txt *.elf *.bin *.strip *.map
	$(RM) *.log *.out bochs* sos.gz
	$(RM) bootstrap/*.o bootstrap/*~
	$(RM) drivers/*.o drivers/*~
	$(RM) hwcore/*.o hwcore/*~
	$(RM) sos/*.o sos/*~
	$(RM) support/*~
	$(RM) extra/*~
	$(MAKE) -C extra clean
	$(MAKE) -C userland clean
